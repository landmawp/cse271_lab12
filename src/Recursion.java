/**
 * A class used to show-case GITLab functionality, and containing two
 * methods used recursively 
 * 
 * @author william landman
 *
 */
public class Recursion {
    
    /**
     * A simple recursive method used to calculate the value of a base
     * number raised to a specific value.
     * 
     * @param base the number being raised to a specific power.
     * @param n the power in which the base is being raised.
     * @return the result of raising the base to said power.
     */
    public static int powerN(int base, int n) {
        // Adding the base case to the method, if n is zero, return 1.
        if (n == 0) {
            return 1;
        }
        // Otherwise multiply the base by a recursive call in which n is
        // decreased by 1.
        return base * powerN(base, n - 1);
    }
    
    /**
     * A simple recursive method, used to calculate how many blocks
     * are in a triangle with row number of rows.
     * 
     * @param row the amount of rows in the triangle.
     * @return the amount of blocks in the triangle.
     */
    public static int triangle(int row) {
        // Adding the base case to the method. If the row is zero or less,
        // return 0.
        if (row <= 0) {
            return 0;
        }
        // Otherwise add the row to a recursive call of the method, in which row
        // is decreased by 1.
        return row + triangle(row - 1);
    }
}
